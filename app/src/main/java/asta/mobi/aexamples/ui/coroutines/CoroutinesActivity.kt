package asta.mobi.aexamples.ui.coroutines

import android.os.Bundle
import asta.mobi.aexamples.R
import asta.mobi.aexamples.base.BaseBindingActivity
import asta.mobi.aexamples.databinding.ActivityCoroutinesBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class CoroutinesActivity : BaseBindingActivity<ActivityCoroutinesBinding>() {

    override val layoutId: Int
        get() = R.layout.activity_coroutines

    private val coroutineScope = CoroutineScope(Dispatchers.Default)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initListeners()
    }

    private fun initListeners() {
        binding.btnLike.setOnClickListener {
            startProcess()
        }
    }

    private suspend fun getDocumentIds(): List<String> =
        suspendCoroutine { continuation ->
            val ids = mutableListOf<String>()
            repeat(100) {
                ids.add("id: $it")
            }
            continuation.resume(ids)
        }

    private fun startProcess() {
        coroutineScope.launch {
            val ids = getDocumentIds()
            repeat(ids.size) {
                launch {
                    println(ids[it])
                }
            }
        }
    }
}