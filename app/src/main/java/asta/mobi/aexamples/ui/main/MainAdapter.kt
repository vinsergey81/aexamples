package asta.mobi.aexamples.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import asta.mobi.aexamples.base.BindingHolder
import asta.mobi.aexamples.data.diffs.mainDiffCallback
import asta.mobi.aexamples.data.entities.MainListEntity
import asta.mobi.aexamples.databinding.ItemListMainBinding

class MainAdapter(private val onClick: (name: MainViewModel.MainEnum) -> Unit) :
    ListAdapter<MainListEntity, BindingHolder<ItemListMainBinding>>(mainDiffCallback) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BindingHolder<ItemListMainBinding> =
        BindingHolder(
            ItemListMainBinding.inflate(
                LayoutInflater.from(parent.context),
                parent, false
            )
        )

    override fun onBindViewHolder(holder: BindingHolder<ItemListMainBinding>, position: Int) {
        holder.binding.item = getItem(position)
        holder.binding.root.setOnClickListener {
            onClick(getItem(position).id)
        }
    }
}