package asta.mobi.aexamples.ui.constraint

import asta.mobi.aexamples.R
import asta.mobi.aexamples.base.BaseBindingActivity
import asta.mobi.aexamples.databinding.ActivityConstraintBinding

class ConstraintActivity : BaseBindingActivity<ActivityConstraintBinding>() {

    override val layoutId: Int
        get() = R.layout.activity_constraint
}