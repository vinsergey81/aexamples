package asta.mobi.aexamples.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import asta.mobi.aexamples.R
import asta.mobi.aexamples.base.BaseVMActivity
import asta.mobi.aexamples.databinding.ActivityMainBinding
import asta.mobi.aexamples.ui.activityresult.HomeActivity
import asta.mobi.aexamples.ui.chart.ChartActivity
import asta.mobi.aexamples.ui.constraint.ConstraintActivity
import asta.mobi.aexamples.ui.coroutines.CoroutinesActivity
import asta.mobi.aexamples.ui.navigation.NavigationActivity
import kotlinx.coroutines.launch
import kotlin.reflect.KClass

class MainActivity : BaseVMActivity<MainViewModel, ActivityMainBinding>() {

    override val viewModelClass: KClass<MainViewModel>
        get() = MainViewModel::class
    override val layoutId: Int
        get() = R.layout.activity_main

    private val adapter = MainAdapter {
        when (it) {
            MainViewModel.MainEnum.CONSTRAINT2 -> {
                startActivity(Intent(this, ConstraintActivity::class.java))
            }

            MainViewModel.MainEnum.BOTTOM_NAVIGATION_WITH_FAB -> {
                startActivity(Intent(this, NavigationActivity::class.java))
            }

            MainViewModel.MainEnum.CHART -> {
                startActivity(Intent(this, ChartActivity::class.java))
            }

            MainViewModel.MainEnum.ACTIVITY_RESULT -> {
                startActivity(Intent(this, HomeActivity::class.java))
            }

            MainViewModel.MainEnum.COROUTINES -> {
                startActivity(Intent(this, CoroutinesActivity::class.java))
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val dividerItemDecoration = DividerItemDecoration(this, RecyclerView.VERTICAL)
        ContextCompat.getDrawable(this, R.drawable.divider_drawable)
            ?.let { dividerItemDecoration.setDrawable(it) }
        binding.listRv.addItemDecoration(dividerItemDecoration)

        binding.listRv.adapter = adapter

        intent.extras?.let {
            showMessage(it.getString("checkId"))
        }

        initObserver()
    }

    private fun initObserver() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.CREATED) {
                viewModel.mainList.collect {
                    adapter.submitList(it)
                }
            }
        }
    }
}