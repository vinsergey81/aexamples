package asta.mobi.aexamples.ui.main

import asta.mobi.aexamples.R
import asta.mobi.aexamples.base.BaseViewModel
import asta.mobi.aexamples.data.entities.MainListEntity
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class MainViewModel : BaseViewModel() {

    private val _mainList = MutableStateFlow<List<MainListEntity>>(listOf())
    val mainList: StateFlow<List<MainListEntity>> = _mainList

    init {
        getMainList()
        showMessage(R.string.app_name)
    }

    private fun getMainList() {
        val list = mutableListOf<MainListEntity>()

        list.add(
            MainListEntity(
                MainEnum.BOTTOM_NAVIGATION_WITH_FAB,
                "Bottom navigation with fab",
                "https://www.youtube.com/watch?v=x6-_va1R788",
                "09.02.2023"
            )
        )
        list.add(
            MainListEntity(
                MainEnum.CONSTRAINT2,
                "Constraint Example",
                "https://www.youtube.com/watch?v=yZYOX6hRYFc",
                "04.12.2022"
            )
        )
        list.add(
            MainListEntity(
                MainEnum.CHART,
                "Custom Chart Example",
                "chatGPT",
                "15.02.2023"
            )
        )
        list.add(
            MainListEntity(
                MainEnum.ACTIVITY_RESULT,
                "ActivityResultContracts Example",
                "https://developer.android.com/reference/androidx/activity/result/contract/ActivityResultContracts",
                "07.07.2021"
            )
        )
        list.add(
            MainListEntity(
                MainEnum.COROUTINES,
                "Coroutines Example",
                "Coroutines",
                "01.02.2024"
            )
        )

        _mainList.value = list
    }

    enum class MainEnum {
        CONSTRAINT2,
        BOTTOM_NAVIGATION_WITH_FAB,
        CHART,
        ACTIVITY_RESULT,
        COROUTINES
    }
}