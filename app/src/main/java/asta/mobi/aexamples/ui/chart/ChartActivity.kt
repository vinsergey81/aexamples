package asta.mobi.aexamples.ui.chart

import android.os.Bundle
import asta.mobi.aexamples.R
import asta.mobi.aexamples.base.BaseBindingActivity
import asta.mobi.aexamples.databinding.ActivityChartBinding
import kotlin.random.Random


class ChartActivity : BaseBindingActivity<ActivityChartBinding>() {

    override val layoutId: Int
        get() = R.layout.activity_chart

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        generateRandomNumbers()

        binding.btnGenerate.setOnClickListener {
            generateRandomNumbers()
        }
    }

    private fun generateRandomNumbers() {
        val numbers = mutableListOf<Float>()
        repeat(10) {
            numbers.add(Random.nextInt(1, 11).toFloat())
        }
        binding.chart.setData(numbers)
    }
}