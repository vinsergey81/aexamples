package asta.mobi.aexamples.ui.activityresult

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import asta.mobi.aexamples.R
import asta.mobi.aexamples.ui.activityresult.HomeActivity

class SomeActivity: Activity() {

    private lateinit var button: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_some)

        val data = intent.extras?.getString(HomeActivity.code)
        Toast.makeText(this, data, Toast.LENGTH_SHORT).show()

        button = findViewById(R.id.button)

        button.setOnClickListener {
            val intent = Intent()
            intent.putExtra(HomeActivity.extra, "ExtraString")
            setResult(RESULT_OK, intent)
            finish()
        }
    }
}