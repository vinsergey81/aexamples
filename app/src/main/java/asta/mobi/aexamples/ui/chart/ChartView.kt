package asta.mobi.aexamples.ui.chart

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.View
import asta.mobi.aexamples.R

class ChartView
@JvmOverloads constructor(
    context: Context,
    private val attrs: AttributeSet? = null,
    defStyle: Int = 0
) : View(context, attrs, defStyle) {

    private val linePaint = Paint()
    private val middleLinePaint = Paint()
    private val dataPoints = mutableListOf<Float>()

    init {
        initTypedArray()
    }

    private fun initTypedArray() {
        middleLinePaint.apply {
            color = Color.BLUE
            strokeWidth = 2f
            style = Paint.Style.STROKE
        }
        if (attrs == null) return
        val ta = context.obtainStyledAttributes(attrs, R.styleable.ChartView, 0, 0)
        linePaint.apply {
            color = ta.getColor(R.styleable.ChartView_chartColor, Color.BLUE)
            strokeWidth = ta.getInteger(R.styleable.ChartView_chartWidth, 5).toFloat()
            style = Paint.Style.STROKE
        }

        ta.recycle()
    }

    fun setData(points: List<Float>) {
        this.dataPoints.clear()
        this.dataPoints.addAll(points)
        invalidate()
    }

    /* @SuppressLint("DrawAllocation")
     override fun onDraw(canvas: Canvas) {
         super.onDraw(canvas)
         // Draw chart using the canvas object

         // Draw a line chart
         val dataPoints = listOf(1f, 2f, 4f, 3f, 5f, 2f, 1f)
         val chartWidth = width - paddingLeft - paddingRight
         val chartHeight = height - paddingTop - paddingBottom
         val xStep = chartWidth / (dataPoints.size - 1).toFloat()

         val path = Path()
         path.moveTo(
             paddingLeft.toFloat(),
             height.toFloat() - paddingBottom.toFloat() - (dataPoints.first() / dataPoints.maxOrNull()!!) * chartHeight
         )
         for (i in 1 until dataPoints.size) {
             path.lineTo(
                 paddingLeft.toFloat() + i * xStep,
                 height.toFloat() - paddingBottom.toFloat() - (dataPoints[i] / dataPoints.maxOrNull()!!) * chartHeight
             )
         }

         canvas.drawPath(path, linePaint)
     }*/

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        // Draw a line chart with numbers
        val chartWidth = width - paddingLeft - paddingRight
        val chartHeight = height - paddingTop - paddingBottom - 50
        val xStep = chartWidth / (dataPoints.size - 1).toFloat()
        val middleValue = getMiddleValue(dataPoints)

        val pathMiddle = Path()

        pathMiddle.moveTo(
            paddingLeft.toFloat(),
            (chartHeight / 2).toFloat()
        )
        pathMiddle.lineTo(
            width.toFloat(),
            (chartHeight / 2).toFloat()
        )

        canvas.drawPath(pathMiddle, middleLinePaint)
        val textMiddle = Paint().apply {
            color = Color.BLUE
            textSize = 36f
        }
        canvas.drawText(
            String.format("%.2f", middleValue).replace(",", "."),
            10f,
            (chartHeight / 2).toFloat() - 20,
            textMiddle
        )


        /*val path = Path()

        path.moveTo(
            paddingLeftCurrent.toFloat(),
            height.toFloat() - paddingBottom.toFloat() - (dataPoints.first() / dataPoints.maxOrNull()!!) * chartHeight
        )
        dataPoints.forEachIndexed { i, _ ->
            path.lineTo(
                paddingLeftCurrent.toFloat() + i * xStep,
                height.toFloat() - paddingBottom.toFloat() - (dataPoints[i] / dataPoints.maxOrNull()!!) * chartHeight
            )

            // Draw number at data point
            val textPaint = Paint().apply {
                color = Color.RED
                textSize = 36f
            }
            val x = paddingLeftCurrent.toFloat() + i * xStep - 10
            val numberPosition = if (dataPoints[i] != dataPoints.last() && dataPoints[i] < dataPoints[i + 1]) {
                -40f
            } else {
                20f
            }
            val y =
                height.toFloat() - paddingBottom.toFloat() - (dataPoints[i] / dataPoints.maxOrNull()!!) * chartHeight - numberPosition
            canvas.drawText(dataPoints[i].toInt().toString(), x, y, textPaint)
        }

        canvas.drawPath(path, linePaint)*/

        val path = Path()
        path.moveTo(paddingLeft.toFloat(), height.toFloat() - paddingBottom.toFloat() - (dataPoints.first() / dataPoints.maxOrNull()!!) * chartHeight)
        for (i in 1 until dataPoints.size) {
            val x1 = paddingLeft.toFloat() + (i - 1) * xStep
            val y1 = height.toFloat() - paddingBottom.toFloat() - (dataPoints[i - 1] / dataPoints.maxOrNull()!!) * chartHeight
            val x2 = paddingLeft.toFloat() + i * xStep
            val y2 = height.toFloat() - paddingBottom.toFloat() - (dataPoints[i] / dataPoints.maxOrNull()!!) * chartHeight
            val cpx = (x1 + x2) / 2
            val cpy1 = y1 + (y2 - y1) * 0.2f
            val cpy2 = y2 - (y2 - y1) * 0.2f
            path.cubicTo(cpx, cpy1, cpx, cpy2, x2, y2)

            // Draw number at data point
            val textPaint = Paint().apply {
                color = Color.RED
                textSize = 36f
            }
            val x = paddingLeft.toFloat() + i * xStep
            val numberPosition = if (dataPoints[i] != dataPoints.last() && dataPoints[i] < dataPoints[i + 1]) {
                -40f
            } else {
                20f
            }
            val y =
                height.toFloat() - paddingBottom.toFloat() - (dataPoints[i] / dataPoints.maxOrNull()!!) * chartHeight - numberPosition
            canvas.drawText(dataPoints[i].toInt().toString(), x, y, textPaint)
        }

        canvas.drawPath(path, linePaint)
    }

    private fun getMiddleValue(list: List<Float>): Float {
        if (list.isEmpty()) return 0f
        var sum = 0f
        list.forEach {
            sum += it
        }
        return sum / list.count()
    }
}