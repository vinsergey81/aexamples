package asta.mobi.aexamples.ui.navigation

import android.os.Bundle
import asta.mobi.aexamples.R
import asta.mobi.aexamples.base.BaseBindingActivity
import asta.mobi.aexamples.databinding.ActivityNavigationBinding

class NavigationActivity : BaseBindingActivity<ActivityNavigationBinding>() {

    override val layoutId: Int
        get() = R.layout.activity_navigation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.bottomNavigationView.background = null
        binding.bottomNavigationView.menu.getItem(2).isEnabled = false
    }
}