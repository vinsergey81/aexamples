package asta.mobi.aexamples.ui.activityresult

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import asta.mobi.aexamples.R

class HomeActivity : AppCompatActivity(R.layout.activity_home) {

    companion object {
        const val code = "code"
        const val extra = "extra"
    }

    private lateinit var button: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        button = findViewById(R.id.button_start)

        button.setOnClickListener {
            openSomeActivityForResult()
        }
    }

    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                // There are no request codes
                val data: Intent? = result.data
                val string = data?.getStringExtra(extra)
                Toast.makeText(this, string, Toast.LENGTH_SHORT).show()
            }
        }

    private val getContent = registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
        // Handle the returned Uri
        Toast.makeText(this, uri.toString(), Toast.LENGTH_SHORT).show()
    }

    private fun openSomeActivityForResult() {
       /* val intent = Intent(this, SomeActivity::class.java).apply {
            putExtra(code, "Result data")
        }
        resultLauncher.launch(intent)*/
        getContent.launch("image/*")
    }
}