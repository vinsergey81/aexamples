package asta.mobi.aexamples.data.diffs

import androidx.recyclerview.widget.DiffUtil
import asta.mobi.aexamples.data.entities.MainListEntity

val mainDiffCallback = object : DiffUtil.ItemCallback<MainListEntity>() {
    override fun areItemsTheSame(oldItem: MainListEntity, newItem: MainListEntity): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: MainListEntity, newItem: MainListEntity): Boolean =
        oldItem == newItem
}