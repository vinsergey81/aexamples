package asta.mobi.aexamples.data.entities

import asta.mobi.aexamples.ui.main.MainViewModel

data class MainListEntity(
    val id: MainViewModel.MainEnum = MainViewModel.MainEnum.CONSTRAINT2,
    val name: String = "",
    val desc: String = "",
    val date: String = ""
)