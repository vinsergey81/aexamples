package asta.mobi.aexamples.base

import androidx.annotation.StringRes

sealed class UiMessage {
    data class Text(val message: String?): UiMessage()
    data class Res(@StringRes val res: Int): UiMessage()
}