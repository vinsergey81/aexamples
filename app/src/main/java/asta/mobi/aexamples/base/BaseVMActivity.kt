package asta.mobi.aexamples.base

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.lifecycleScope
import asta.mobi.aexamples.BR
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.getViewModel
import kotlin.reflect.KClass

abstract class BaseVMActivity<VM : BaseViewModel, B : ViewDataBinding> : BaseBindingActivity<B>() {

    protected abstract val viewModelClass: KClass<VM>
    protected lateinit var viewModel: VM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = getViewModel(clazz = viewModelClass)
        binding = DataBindingUtil.setContentView(this, layoutId)
        binding.setVariable(BR.viewModel, viewModel)
        lifecycleScope.launch {
            viewModel.messageData.collect { message ->
                when (message) {
                    is UiMessage.Res -> {
                        showMessage(message.res)
                    }

                    is UiMessage.Text -> {
                        showMessage(message.message)
                    }
                }
            }
        }
    }

    fun hideKeyboard(view: View) {
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}