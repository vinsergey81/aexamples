package asta.mobi.aexamples.base

import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

open class BaseViewModel : ViewModel() {

    private val _messageData = Channel<UiMessage>()
    val messageData: Flow<UiMessage> = _messageData.receiveAsFlow()

    /**
     * Shows message by string
     * @param message
     */
    fun showMessage(message: String?) {
        viewModelScope.launch {
            _messageData.send(UiMessage.Text(message))
        }
    }

    /**
     * Shows message by string resource
     * @param messageId
     */
    fun showMessage(@StringRes messageId: Int) {
        viewModelScope.launch {
            _messageData.send(UiMessage.Res(messageId))
        }
    }
}