package asta.mobi.aexamples.di

import asta.mobi.aexamples.ui.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel<MainViewModel>()
}